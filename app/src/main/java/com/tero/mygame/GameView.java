package com.tero.mygame;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;


public class GameView extends View {

    Handler handler;
    Runnable runnable;
    final int UPDATE_MILLS = 25;
    Bitmap background;
    Display display;
    Point point;
    Rect rect;

    Bitmap[] terop;
    int teroFrame = 0, velocity=0,gravity=3;
    int dWidth, dHeight, teroX, teroY;

    public GameView(Context context) {
        super(context);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };

        background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        display = ((Activity)getContext()).getWindowManager().getDefaultDisplay();
        point = new Point();
        display.getSize(point);
        dWidth = point.x;
        dHeight = point.y;
        rect = new Rect(0,0,dWidth,dHeight);
        terop = new Bitmap[4];
        terop[0] = BitmapFactory.decodeResource(getResources(),R.drawable.terodactyl1);
        terop[1] = BitmapFactory.decodeResource(getResources(),R.drawable.terondactyl2);
        terop[2] = BitmapFactory.decodeResource(getResources(),R.drawable.terodactyl3);
        terop[3] = BitmapFactory.decodeResource(getResources(),R.drawable.terondactyl4);

        teroX = 60;
        teroY = dHeight/2 - terop[0].getHeight()/2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(background,null,rect,null);

        if (teroFrame == 0){
            teroFrame = 1;
        } else if (teroFrame == 1) {
            teroFrame = 2;
        } else if (teroFrame == 2){
            teroFrame = 3;
        } else if (teroFrame == 3){
            teroFrame = 0;
        }

            if (teroY < dHeight - terop[0].getHeight() || velocity < 0){
                velocity += gravity;
                teroY += velocity;
                Log.d("HEIGTH", String.valueOf(teroY));
                Log.d("VELOCITY", String.valueOf(velocity));
            }

        canvas.drawBitmap(terop[teroFrame],teroX,teroY,null);

        handler.postDelayed(runnable, UPDATE_MILLS);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

         if (teroY > 10){
             if (action == MotionEvent.ACTION_DOWN){
                 velocity = -50;
             }
         } else {
             velocity = 0;
         }
         return true;
    }
}
